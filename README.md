# Statistical learning

## Wiki : [First things to do](https://framagit.org/kzagalo/IMC-4302C/-/wikis/First-things-to-do)

## Outline

0. [Introduction to Python](Lab0)
1. [Linear regression](Lab1)
1. [Linear classification](Lab2)
1. [Model selection](Lab3)
1. [Support vector machines](Lab4)
1. [k-nearest neighbors](Lab5)

## Final project [instructions](project.pdf)
